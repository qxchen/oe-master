import {AddSavingAttributes} from "./handlers/fileHandlers/AddSavingAttributes";
import {CheckJson} from "./handlers/fileHandlers/CheckJson";
import {BindKeys} from "./handlers/fileHandlers/BindKeys";
import {AddSummaryKeys} from "./handlers/fileHandlers/AddSummaryKeys";
import {AllParams} from "./utils/ParamsGetter";

type FileHandlers =
    typeof AddSavingAttributes |
    typeof CheckJson |
    typeof BindKeys |
    typeof AddSummaryKeys

type CrawlerHandlers = ''

interface FileHandlerConstructor {
    new(params: AllParams): FileHandler
}

interface FileHandler {
    run(filePath: string): void;

    developing?: true
}

interface CrawlerHandler {
    run(url?: string): void
}

function newHandler(constructor: FileHandlerConstructor, params: AllParams): FileHandler {
    return new constructor(params)
}

interface OEJSONItem {
    pos: [number, number],
    content: string,
    rule: string,
    title?: string
}

interface OEJSON {
    [key: string]: {
        [key: string]: OEJSONItem
    }
}

interface OEJSONOld {
    [key: string]: {
        [key: string]: {
            [key: string]: OEJSONItem
        }
    }
}

interface Master {
    run: () => void
}


export {Master, FileHandlers, FileHandlerConstructor, newHandler, FileHandler, OEJSON, OEJSONOld, OEJSONItem}
