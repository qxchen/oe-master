import {Master} from "../types";
import inquirer from "inquirer";
import {ConvertPdfToVue} from "../handlers/crawlerHandlers/ConvertPdfToVue";

class CrawlerMaster implements Master {
    async getHandler(): Promise<any> {
        const {handler} = await inquirer.prompt([
            {
                name: 'handler',
                message: 'what do u want to do?',
                type: 'list',
                choices: [
                    {
                        name: 'Generate OE structure . This handler will create vue template and static files .',
                        value: ConvertPdfToVue
                    }
                ]
            }
        ])
        return handler
    }

    async run() {
        const Handler = await this.getHandler()
        const handler = new Handler()
        handler.run()
    }
}

export {CrawlerMaster}
