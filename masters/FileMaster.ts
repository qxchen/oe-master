import {AllParams, ParamsGetter} from "../utils/ParamsGetter";
import {newHandler, Master} from "../types";

const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

class FileMaster implements Master{
    constructor(private entry: string) {
    }

    private async getParams() {
        const paramsGetter = new ParamsGetter(this.entry)
        return await paramsGetter.get()
    }

    private getFilePaths(Type: string): string[] {
        if (fs.statSync(this.entry).isFile()) {
            return [this.entry]
        } else {
            return fs.readdirSync(this.entry).map((fileName: string) => {
                if (path.extname(fileName) === Type) {
                    return path.join(this.entry, fileName)
                } else {
                    console.log(`skipped file: ${chalk.red(fileName)} .`)
                }
                return false
            }).filter((filePath: string | false) => filePath) // 去空
        }
    }

    private static handleFiles(filePaths: string[], params: AllParams) {
        const h = newHandler(params.Handler, params)
        filePaths.forEach(filePath => {
            if(path.extname(filePath) === params.Type){
                h.run(filePath)
            }else{
                console.log(`FileType not match . Skipped: ${chalk.red(filePath)}`)
            }
        })
    }

    async run() {
        // 设定参数
        const params = await this.getParams()
        // 读取文件
        const filePaths = this.getFilePaths(params.Type)
        // 处理/写入文件
        FileMaster.handleFiles(filePaths, params)
    }
}

export {FileMaster}
