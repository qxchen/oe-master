import {OEJSON} from "../types";

const replaceJsonKey = (json: OEJSON, workingJson: OEJSON, oldKey: string, newKey: string): OEJSON => {
    for (let pKey in json) {
        if (json.hasOwnProperty(pKey)) {
            const target = json[pKey][oldKey]
            if (target) {
                json[pKey][newKey] = JSON.parse(JSON.stringify(json[pKey][oldKey]))
                delete json[pKey][newKey].title
                if(workingJson[pKey]){
                    if(workingJson[pKey][newKey]){
                        json[pKey][newKey].rule = workingJson[pKey][newKey].rule
                    }else{
                        console.log(`${pKey}:${newKey} was not found in working json file , skipped .`)
                    }
                }
                delete json[pKey][oldKey]
                break;
            }
        }
    }
    return json
}

export {replaceJsonKey}
