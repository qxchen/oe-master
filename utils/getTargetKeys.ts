import {OEJSON} from "../types";
import {questionOfGetTarget} from './questionOfGetTarget'

const getTargetKeys = async (json: OEJSON) => {
    const targetKeys: string[] = []
    const {targetKeyRules, useReg} = await questionOfGetTarget()
    const reg = useReg ? new RegExp(targetKeyRules) : null
    const arr = useReg ? [] : JSON.parse(targetKeyRules)
    for (let pageKey in json) {
        if (json.hasOwnProperty(pageKey)) {
            for (let key in json[pageKey]) {
                if (json[pageKey].hasOwnProperty(key)) {
                    if (useReg) {
                        reg && reg.test(key) && targetKeys.push(key)
                    } else {
                        arr.includes(key) && targetKeys.push(key)
                    }
                }
            }
        }
    }
    return targetKeys
}

export {getTargetKeys}
