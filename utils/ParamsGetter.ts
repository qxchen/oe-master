import {Answers, Question} from "inquirer";
import {AddSavingAttributes} from "../handlers/fileHandlers/AddSavingAttributes";
import {CheckJson} from "../handlers/fileHandlers/CheckJson";
import {BindKeys} from "../handlers/fileHandlers/BindKeys";
import {AddSummaryKeys} from "../handlers/fileHandlers/AddSummaryKeys";
import {TranslateOldJson} from "../handlers/fileHandlers/TranslateOldJson";
import {Stats} from "fs";
import {FileHandlerConstructor} from "../types";
import {AddPopulateRule} from "../handlers/fileHandlers/AddPopulateRule";
import {LowerCaseKeys} from "../handlers/fileHandlers/LowerCaseKeys";
import {FixSelectValues} from "../handlers/fileHandlers/FixSelectValues";
import {ParseNewJsonToOld} from "../handlers/fileHandlers/ParseNewJsonToOld";
import {AddSubKeys} from "../handlers/fileHandlers/AddSubKeys";
import {ChangePosition} from "../handlers/fileHandlers/ChangePosition";
import {ChangePositionByIdJsonAndVue} from "../handlers/fileHandlers/ChangePositionByIdJsonAndVue";

const inquirer = require('inquirer')
const chalk = require('chalk')
const fs = require('fs')
const path = require('path')

interface AllParams {
    Type: '.vue' | '.json',
    Handler: FileHandlerConstructor,
    Rewrite: boolean
}

class ParamsGetter {
    private entryStat: Stats

    constructor(private entry: string) {
        this.entryStat = fs.statSync(entry)
    }


    private static getQuestions(Type: string): Question[] {
        let typeQuestions
        if (Type === '.vue') {
            typeQuestions = [
                {name: 'Bind keys of a vue template .', value: BindKeys},
                {name: 'Add auto-save attributes to form controls .', value: AddSavingAttributes},
                {name: 'Fix abbreviated value of select .', value: FixSelectValues},
            ]
        } else {
            typeQuestions = [
                {name: 'Check json file .', value: CheckJson},
                {name: 'Add summary keys', value: AddSummaryKeys},
                {name: 'Add sub keys', value: AddSubKeys},
                {name: 'Translate old json with "part" key to new json .', value: TranslateOldJson},
                {name: 'Set populate-filter like g28Value to rules .', value: AddPopulateRule},
                {name: 'Lower all keys . For Example: 1.B.3.C -> 1.b.3.c .', value: LowerCaseKeys},
                {
                    name: 'Parse id-based json to title-based json , through a completed vue template .',
                    value: ParseNewJsonToOld
                },
                {
                    name: 'Fill id-based json file\'s position to working json file .',
                    value: ChangePositionByIdJsonAndVue
                },
                {name: 'Change position .', value: ChangePosition},
            ]
        }
        const questions = [
            {
                type: 'list',
                name: 'Handler',
                message: 'What u want to do ?',
                choices: typeQuestions
            },
            {
                type: 'list',
                name: 'Rewrite',
                message: 'How to save the handle result (if any)?',
                choices: [
                    {name: 'Save to a new file with _tmp suffix.', value: false},
                    {name: `Save and ${chalk.red('OVERWRITE')} origin file`, value: true}
                ]

            }
        ]
        return questions
    }

    private async askForParams(): Promise<AllParams> {
        const defaultAnswers: Answers = {}
        if (this.entryStat.isFile()) {
            defaultAnswers.Type = path.extname(this.entry)
        } else {
            defaultAnswers.Type = (await inquirer.prompt([
                {
                    type: 'list',
                    name: 'Type',
                    message: 'Which type of file u want to handle ? ',
                    choices: [{name: '*.vue', value: '.vue'}, {name: '*.json', value: '.json'}]
                }
            ])).Type
        }

        const answers: AllParams = await inquirer.prompt(ParamsGetter.getQuestions(defaultAnswers.Type))
        return {
            ...defaultAnswers,
            ...answers
        }
    }

    async get(): Promise<AllParams> {
        return await this.askForParams()
    }
}

export {ParamsGetter, AllParams}
