const request = require('request');
const fs = require('fs');


const upload = async (url: string, filePath: string, fileName: string): Promise<any> => {
    const options = {
        method: 'POST',
        url,
        formData: {
            'file': {
                'value': fs.createReadStream(filePath),
                'options': {
                    'filename': `${fileName}.pdf`,
                    'contentType': null
                }
            }
        },
        headers: {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8",
            "Access-Control-Request-Headers": "access-control-allow-origin",
            "Access-Control-Request-Method": "POST",
            "Connection": "keep-alive",
            "Host": "converter.idrsolutions.com",
            "Origin": "https://www.idrsolutions.com",
            "Referer": "https://www.idrsolutions.com/",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-site",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/92.0.4515.131 Safari/537.36"
        }
    }
    return new Promise((resolve, reject) => {
        request(options, function (error: Error, response: any) {
            if (error) {
                reject(error)
            } else {
                resolve(response)
            }
        });
    })
}

export {upload}
