import exp from "constants";

const inquirer = require('inquirer')

const questionOfGetTarget = async (): Promise<{ targetKeyRules: string, useReg: boolean, useSelector: boolean }> => {
    const answers = await inquirer.prompt([
        {
            type: 'list',
            name: 'KeyMatch',
            message: 'How do you want to describe keys/ids to handle ?',
            choices: [
                {name: 'Use keys/ids Array.', value: 'Array'},
                {name: 'Use RegExp.', value: 'RegExp'},
                {name: 'Use jQuery selector . (handle vue only)', value: 'Selector'}
            ]
        }
    ])
    const question = {
        type: 'string',
        name: 'targetKeyRules',
        message: ''
    }
    const useReg = answers.KeyMatch === 'RegExp'
    const useSelector = answers.KeyMatch === 'Selector'
    if (useReg) {
        question.message = 'Please enter a RegExp in Javascript syntax  (without `/`). All keys matched will be handled. (For example: ^8.[34567].d$) :'
    } else if (useSelector) {
        question.message = 'Please enter a selector string in jQuery selector syntax . Like #form_13 , selecter, input , etc... :'
    } else {
        question.message = 'Please enter a list of keys u want to handle , it should be an string array like ["1.1","1.2","1.3"] or ["#form1_3","#form2_3"] (DO NOT USE SINGLE QUOT!) : '
    }
    const {targetKeyRules} = await inquirer.prompt([question])
    return {targetKeyRules, useReg, useSelector}
}

export {questionOfGetTarget}
