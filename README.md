# OE Master

## Attention

Use single tag element carefully .

```

<!-- fine -->
<br />
<img />
<input />
<a href="google.com"></a>

<!-- wrong -->
<a href="google.com" />

```

## Description

This is a tool box for OnlineEditor . It provides some commandline tools to handle `vue` template and `json` file .
Available tools are:

vue:

- AddSavingAttributes . Add auto-save attributes to original vue template without auto-save feature .
- BindKeys . Analyze `title` to bind keys to vue template . 60% hit ratio .
- Fix abbreviated value of `select` .

json:

- CheckJson . Check if the json file is with error . Missing `rule` , `pos` or `content` . Also check repetitive keys .
- AddSummaryKeys. Add summary keys to split-field and multiple-line items.
- Translate old json (with 'part' keys) to new json (without 'part' key)
- Set all rules to populate filter like g28Value .

structure:

- Generate Vue component and static files by a form name . @/handlers/crawlerHandlers/ConvertPdfToVue.md

developing more ... comming soon .

## Installation

```
    npm install -g oe-master

    // or you are using yarn

    yarn global add oe-master
```

## How to use

### 生成vue模板

直接运行，不需要其他参数

```

oe-master

```

### 处理某个特定的文件比如要处理json或者已有的vue模板

需要用entry参数来指定要处理的文件

```

    entry=./g28.vue oe-master
    
```

entry can be :

- a file path .
- a directory contains vue or json files.

Interactive command , no documents needed.

## Screenshots
![Image text](https://bitbucket.org/qxchen/oe-master/raw/e7cf84cc72dcae9fc7654f640ecf56e3998579f3/readme/commandline.png)
