import cheerio from 'cheerio'

const checkedType = ['checkbox', 'radio']

export default function convert(html: string, formName: string) {
  const $ = cheerio.load(html, {
    ignoreWhitespace: true,
    normalizeWhitespace: false,
  })
  const body = $('body')

  const styleNodes = body.find('style')
  styleNodes.remove()

  const style = Array.from(styleNodes).reduce(
    (sum, cur) => sum + $(cur).html(),
    ''
  )

  const pageAreas = body
    .find(".pageArea")
    .map((_, e) => $.html(e))
    .toArray()
    .join("\n")

  body.html(pageAreas)

  $('div.t').remove()
  $('script').remove()

  $('textarea')
    .removeAttr('disabled')
    .removeAttr('value')
    .attr(':value', 'valueOf(key__)')
    .attr('@change', 'handleInput(key__, $event)')

  $('select')
    .removeAttr('disabled')
    .removeAttr('value')
    .attr(':value', 'valueOf(key__)')
    .attr('@change', 'handleInput(key__, $event)')

  $('input')
    .removeAttr('disabled')
    .removeAttr('value')
    .attr(':value', 'valueOf(key__)')
    .attr('@change', 'handleInput(key__, $event)')
    .filter((_, el) => checkedType.includes($(el).attr('type')))
    .removeAttr(':value')
    .removeAttr('@change')
    .each((_, el) => {
      const $el = $(el)
      const src = $el.attr('imagename')
      const id = $el.attr('id')
      const img = $(
          // /static/formControlImages/R
        '<img id="' +
          id +
          '_img" :src="`/static/formControlImages/R' +
          '${stateOf(key__)}`" @click="handleCheck(key__)" data-input-id="' +
          id +
          '" />'
      )

      img.insertBefore($el)
    })

  $('object').each((_, el) => {
    const src = $(el).attr('data')
    const img = $(`<img src="/static/${formName.replace('-','')}/${src}" />`)

    $(el).removeAttr('data')
    img.attr($(el).attr())

    $(el).replaceWith(img)
  })

  return {
    style: style.replace(
      /url\(['"](.*)['"]\)/g,
      (_, url) => `url("/static/${formName}/${url}")`
    ),
    dom: body
      .html()
      .replace(/(<!--.*?-->)/g, '')
      .trim(),
  }
}
