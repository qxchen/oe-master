import fs from 'fs'

export default function load(path: string) {
  return fs.readFileSync(path, { encoding: 'utf8' })
}
