import fs from 'fs'
import {resolve} from 'path'
import load from './load'
import convert from './convert'
import generate from './generate'

const prettier = require('prettier')

const generateComponent = (formHtmlDir: string, formName: string, destDir: string) => {
    const html = load(resolve(formHtmlDir, 'form.html'))
    const {dom, style} = convert(html, formName)
    const component = generate(dom, style)
    console.log('Formatting file , please wait ...')
    fs.writeFileSync(resolve(__dirname, destDir, `${formName}.vue`), prettier.format(component, {parser: 'vue'}))
}

export {generateComponent}
