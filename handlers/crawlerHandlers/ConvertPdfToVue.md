# Convert PDF to Vue

## Check version 

oe-master version should be later than `1.1.4`

```
oe-master -v
```

## Run command

```
oe-master 
# with no 'entry' option 
```

## Choose Tool

Select this : `Generate OE structure . This handler will create vue template and static files .`

## Set path

Give ABSOLUTE PATH of ailaw project . If u are already in ailaw project path . Just press Enter .

## Set form name

Give a form name of USCIS form. This should be same with download url from USCIS website .

For example , download-url of 129f in USCIS website is 
```
https://www.uscis.gov/system/files_force/files/form/i-129f.pdf?download=1
```
You should input `i-129f`. WITHOUT `.pdf` suffix !

Another example, download-url of 539a in USCIS website is

```
https://www.uscis.gov/system/files_force/files/form/i-539a-pc.pdf?download=1
```

You should input `i-539a-pc`.

## Just wait

