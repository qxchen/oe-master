const inquirer = require("inquirer");
const chalk = require('chalk')
const path = require('path')
const download = require('download')
const {upload} = require('../../utils/uploadFile')
const extract = require('extract-zip')
const fs = require('fs')
const axios = require('axios')
import {generateComponent} from "./generateComponent";

class ConvertPdfToVue {
    private ailaw: string = ''
    private pdfName: string = ''

    constructor() {
    }

    get tmpDir(): string {
        return path.join(this.ailaw, 'tmp')
    }

    get formHtmlDir(): string {
        return path.join(this.tmpDir, this.pdfName)
    }

    get ailawFormPath(): string {
        return path.join(this.ailaw, 'src/forms')
    }

    get ailawStaticPath(): string {
        return path.join(this.ailaw, 'public/static')
    }

    async getProjectDir(): Promise<{ ailaw: string }> {
        const {ailaw} = await inquirer.prompt([
            {
                name: 'ailaw',
                message: `Please enter the ABSOLUTE PATH of ${chalk.yellow('ailaw')} project in your computer .`,
                type: 'test'
            }
        ])
        return {ailaw: path.resolve(ailaw)}
    }

    async init() {
        this.ailaw = (await this.getProjectDir()).ailaw
        try {
            fs.readdirSync(this.tmpDir)
        } catch (e) {
            fs.mkdirSync(this.tmpDir)
        }
    }

    async getOriginPdf(): Promise<string> {
        let formUrl = '', pdfName = ''
        pdfName = (await inquirer.prompt([
            {
                name: 'pdfName',
                type: 'text',
                message: 'Please enter Pdf name . I-907 , i907, i-907 are all fine .'
            }
        ])).pdfName
        formUrl = (await inquirer.prompt([
            {
                name: 'formUrl',
                type: 'text',
                message: 'Please enter the full download url. For example: https://some.where.com/download/eoir199'
            }
        ])).formUrl
        if (!/^http(s)?:\/\//.test(formUrl)) {
            console.log(chalk.red('Please enter correct url , start with http or https.'))
            process.exit(1);
        }

        this.pdfName = pdfName
        console.log(`Getting form from ${chalk.yellow(formUrl)}`)
        await download(formUrl, path.join(this.tmpDir), {filename: `${pdfName.replace('.pdf', '')}.pdf`})
        return `${this.tmpDir}/${pdfName}.pdf`
    }

    async askForProcessResult(uuid: string): Promise<{ state: string, downloadUrl: string }> {
        const convertRes = await axios.get(`https://cloud.idrsolutions.com/online-converter/convert?uuid=${uuid}`)
        console.log(uuid, convertRes.data.state)
        if (convertRes.data.state === 'processing') {
            console.log('Processing , try again in 2 seconds .')
            return new Promise(resolve => {
                setTimeout(async () => {
                    resolve(await this.askForProcessResult(uuid))
                }, 2000)
            })
        } else {
            return {state: 'processed', downloadUrl: convertRes.data.downloadUrl}
        }
    }

    // async convertPdfToHtml(pdfPath: string): Promise<string> {
    //     console.log(`Converting pdf ...`)
    //     const uploadUrl = `https://converter.idrsolutions.com/online-converter/convert?toType=html5&input=upload&settings={"org.jpedal.pdf2html.textMode":"svg_shapetext_selectable"}`
    //     // return new Promise(resolve => {
    //     //     setTimeout(() => {
    //     //         const uuid = 'cec7b24d-12fe-49b3-939c-71c37de81896'
    //     //         resolve(uuid)
    //     //     }, 5000)
    //     // })
    //     const res = await upload(uploadUrl, pdfPath, this.pdfName)
    //     const parsedRes = JSON.parse(res.body)
    //     // const parsedRes = {uuid: '266e95fd-0596-4bb9-82a0-462cf2ecd9f9'}
    //     if (parsedRes.uuid) {
    //         const {downloadUrl} = await this.askForProcessResult(parsedRes.uuid)
    //         return downloadUrl
    //     } else {
    //         console.log(parsedRes)
    //         throw Error('Convert fail .')
    //     }
    //
    // }
    //
    // async downloadZip(downloadPath: string): Promise<string> {
    //     console.log(`Downloading zip file from ${downloadPath} ...`)
    //     await download(downloadPath, path.join(this.tmpDir))
    //     const arr = downloadPath.split('/')
    //     const zipFileName = arr[arr.length - 1]
    //     return path.join(this.tmpDir, zipFileName)
    // }

    generateComponent() {
        generateComponent(this.formHtmlDir, this.pdfName.replace('-', ''), this.formHtmlDir)
    }

    removeUselessResources() {
        console.log('Removing useless resources...')
        fs.unlinkSync(path.resolve(this.formHtmlDir, 'form.html'))
        fs.rmdirSync(path.resolve(this.formHtmlDir, 'js'), {recursive: true})
        fs.readdirSync(this.formHtmlDir).map(dirOrFile => {
            if (/^\d+$/.test(dirOrFile)) {
                const pageDirPath = path.resolve(this.formHtmlDir, dirOrFile)
                const isFile = fs.statSync(pageDirPath).isFile()
                if (!isFile) {
                    const formImageDirPath = path.join(pageDirPath, 'form')
                    try {
                        fs.readdirSync(formImageDirPath).forEach(imageName => {
                            fs.unlinkSync(path.join(formImageDirPath, imageName))
                        })
                    } catch (e) {
                        console.log('skip...')
                    }
                }
            }

        })
    }

    moveResultToAilaw() {
        console.log('Moving to ailaw project path ...')
        console.log(this.pdfName)
        const vueFileName = `${this.pdfName.replace('-', '')}.vue`
        fs.renameSync(path.join(this.formHtmlDir, vueFileName), path.join(this.ailawFormPath, vueFileName))
        fs.renameSync(this.formHtmlDir, path.join(this.ailawStaticPath, this.pdfName.replace('-', '')))
        fs.rmdirSync(this.tmpDir, {recursive: true})
    }

    async waitForConvertedZipFile(retriedTime = 0) {
        const max = 180
        console.log(chalk.red('---------------------------------------------------------------------------------------'))
        console.log(`Please paste converted pdf zip file into ${chalk.red(path.join(this.tmpDir))} ${chalk.yellow(`in ${max-retriedTime} seconds`)}, I'm waiting ...`)
        console.log(`Convert Tool:`)
        console.log('https://www.idrsolutions.com/online-pdf-to-html5-converter')
        console.log(`You have to set ${chalk.yellow('Text Mode: Shape Text')} and keep ${chalk.yellow('inline SVGs checked')} in ${chalk.yellow('Advanced options')} when converting.`)
        const result = await new Promise((resolve, reject) => {
            setTimeout(() => {
                const fileNames = fs.readdirSync(path.join(this.tmpDir))
                let fileName = null;
                const zipExisted = fileNames.some(fName => {
                    if (fName.includes('.zip')) {
                        fileName = fName
                        return true
                    }
                })
                if (zipExisted) {
                    resolve(fileName)
                } else if (retriedTime > max) {
                    reject()
                } else {
                    resolve(null)
                }
            }, 1000)
        })
        if (result) {
            return `${path.join(this.tmpDir)}/${result}`
        } else {
            return await this.waitForConvertedZipFile(retriedTime + 1)
        }
    }

    async run() {
        await this.init()
        const thisPath = path.join(this.tmpDir)
        if (fs.readdirSync(thisPath).some(name => name.includes('.zip'))) {
            console.log(chalk.red(`Please remove all zipFiles in ${thisPath}`))
            throw Error()
        }
        await this.getOriginPdf()
        const zipFilePath = await this.waitForConvertedZipFile()
        console.log('Extracting...')
        await extract(zipFilePath, {dir: this.tmpDir})
        await this.generateComponent()
        this.removeUselessResources()
        this.moveResultToAilaw()
    }
}

export {ConvertPdfToVue}
