import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
import {getTargetKeys} from "../../utils/getTargetKeys";

const reverse = (str: string) => {
    return str.split('').reverse().join('')
}

class AddSummaryKeys extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string) {
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const keys = await getTargetKeys(json)
        for (let pageKey in json) {
            const page = json[pageKey]
            for (let key in page) {
                keys.some(summaryKey => {
                        if (`${summaryKey}.1` === key) {
                            const oldRule = page[key].rule
                            page[summaryKey] = {
                                content: '$',
                                pos: [-500, -500],
                                rule: reverse(reverse(oldRule).replace(/\]\d+\[/, ''))
                                    .replace(/\s?\|\s?wrapLongText\(\s?\d+\s?,\s?\d+\s?\)/, '')
                            }
                            return true
                        }
                    }
                )
            }
        }
        this.save(filePath, {content: json})
    }
}

export {AddSummaryKeys}
