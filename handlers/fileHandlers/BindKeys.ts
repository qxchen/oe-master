import {Handler} from "../Handler";
import {FileHandler} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

const chalk = require('chalk')
const cachedKeys: string[] = []
const inquirer = require('inquirer')

class BindKeys extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    static getLastHit = (str: string, reg: RegExp) => {
        reg.lastIndex = 0
        let result = null
        let current: RegExpExecArray | null = null
        // 不应该有 1.e.f 这种编号 并且title中的 U.S. 之类的会造成影响
        const invalidReg = /[A-Z]\.(\s)?[A-Z]/
        while ((current = reg.exec(str)) != null) {
            if (current && invalidReg.test(current[0])) {
                continue
            }
            result = current
        }
        if (!result) console.log(chalk.red('No keywords found in this title , skipped:'), str)
        return result ? result[0] : ''
    }

    static getKeyFromTitle(titleStr: string) {
        const partReg = /Part\s[\d]+\./
        const orderStr = BindKeys.getLastHit(titleStr, /(\s[\dA-Z]+\.)+\s/g)
        const partHit = partReg.exec(titleStr)
        if (!partHit) {
            return 'key__'
        }
        const partStr = partHit[0].split(' ')[1]
        const key = `${partStr}${orderStr}`.replace(/\s/g, '').slice(0, -1).toLowerCase()
        cachedKeys.push(key)
        return `'${key}'`
    }

    static parseIdToKey(id: string) {
        return id.slice(4).split('_').reverse().join('.')
    }

    handleInputItem(ipt: Cheerio, $: CheerioStatic, useId: boolean = false) {
        const $input = $(ipt)
        let key = '';
        const title = $input.attr('title')
        const valueStr = $input.attr(':value')
        if (useId) {
            const id = $input.attr('id');
            key = BindKeys.parseIdToKey(id)
        } else {
            if (title) {
                key = BindKeys.getKeyFromTitle(title)
                if (!valueStr) {
                    console.log(`${$input.attr('id')} has no value , please check...`)
                    return
                }
            }
        }

        $input.attr(':value', valueStr.replace('key__', key))
        const changeEventStr = $input.attr('@change')
        if (changeEventStr) {
            $input.attr('@change', changeEventStr.replace('key__', key))
            $input.attr('key', key.split("'")[1])
        }
    }

    handleCheckBox($checkbox: Cheerio, useId: boolean = false) {
        const title = $checkbox.attr('title')
        const $image = $checkbox.prev()
        const id = $checkbox.attr('id')
        let src = $image.attr(':src')
        let key = ''
        if (useId) {
            key = BindKeys.parseIdToKey(id);
        } else if (title && src) {
            key = BindKeys.getKeyFromTitle(title)
        }
        // 替换image的src地址 解决重复图片的问题
        const indexOfSrcMark = src.indexOf('R${')
        src = src.slice(indexOfSrcMark).replace('R${', '`/static/formControlImages/R${')
        // 替换image的key
        src = src.replace('key__', key)
        // 写入image的src
        $image.attr(':src', src)
        // 写入image的@click事件
        const clickEventStr = $image.attr('@click')
        if (clickEventStr) {
            $image.attr('@click', clickEventStr.replace('key__', key))
            // 操作关联的input
            let checkedStr = $checkbox.attr(':checked')
            if (!checkedStr) {
                checkedStr = 'valueOf(key__)'
            }
            $checkbox.attr(':checked', checkedStr.replace('key__', key))
            $checkbox.attr('key', key.split("'")[1])
        }
    }

    async run(filePath: string) {
        const {useId} = await inquirer.prompt([
            {
                name: 'useId',
                message: 'How to parse the keys ?',
                type: 'list',
                choices: [
                    {name: 'By title content .', value: false},
                    {name: 'By form element id .', value: true},
                ]
            }
        ])
        const {$} = Handler.readPagesAsDom(filePath)
        const $inputs = $('input[type="text"],select')
        const $inputsOfButton = $('input[type="button"]')
        const $textareas = $('textarea')
        const $checkboxes = $('input[type="checkbox"]')
        $inputsOfButton.each((_, ipt) => {
            $(ipt).attr('type', 'text')
            this.handleInputItem($(ipt), $, useId)
        })
        $inputs.each((_, ipt) => {
            this.handleInputItem($(ipt), $, useId)
        })
        $textareas.each((_, ipt) => {
            $(ipt).addClass('multiple-line')
            this.handleInputItem($(ipt), $, useId)
        })
        $checkboxes.each((_, checkBox) => {
            this.handleCheckBox($(checkBox), useId)
        })
        $inputs.removeAttr('key')
        $checkboxes.removeAttr('key')
        $textareas.removeAttr('key')
        $inputsOfButton.removeAttr('key')
        this.save(filePath, {$})
    }
}

export {BindKeys}
