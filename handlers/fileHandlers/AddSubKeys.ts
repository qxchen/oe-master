import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
import {getTargetKeys} from "../../utils/getTargetKeys";
import inquirer from "inquirer";

const reverse = (str: string) => {
    return str.split('').reverse().join('')
}

class AddSubKeys extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string) {
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const keys = await getTargetKeys(json)
        const {isTextarea} = await inquirer.prompt([{
            type: "list",
            name: 'isTextarea',
            choices: [
                {name: 'Yes, I\'m handling multiple-line textarea', value: true},
                {name: 'No, I\'m handling split-field input', value: false}
            ]
        }])
        let minus = false
        if (isTextarea) {
            minus = (await inquirer.prompt([{
                type: 'list',
                name: 'minus',
                choices: [
                    {name: 'No, Current line is last line', value: false},
                    {name: 'Yes, Current line is first line', value: true}
                ]
            }])).minus
        }
        keys.forEach((key) => {
            Object.keys(json).some(pKey => {
                return Object.keys(json[pKey]).some((k) => {
                    if (k === key) {
                        const max = isTextarea ? 10 : 12
                        for (let i = 1; i <= max; i++) {
                            const old = {
                                ...json[pKey][k]
                            }
                            if (isTextarea) {
                                json[pKey][`${k}.${minus ? i : (max + 1 - i)}`] = {
                                    ...old,
                                    pos: [
                                        old.pos[0],
                                        old.pos[1] + ((minus ? -i : i) * 18)
                                    ],
                                    rule: old.rule.replace('}}', ` | wrapLongText(${i - 1})}}`)
                                }
                            } else {
                                json[pKey][`${k}.${i}`] = {
                                    ...old,
                                    pos: [
                                        old.pos[0] + (i * 14),
                                        old.pos[1]
                                    ],
                                    rule: old.rule.replace('}}', `[${i - 1}]}}`)
                                }
                            }
                        }
                        json[pKey][k].pos = [-500, -500]
                        json[pKey][k].content = "$"
                        return true;
                    }
                })
            })
        })
        this.save(filePath, {content: json})
    }
}

export {AddSubKeys}
