import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

class LowerCaseKeys extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    run(filePath: string) {
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        for (let pageKey in json) {
            const page = json[pageKey]
            for (let key in page) {
                const lowerKey = key.toLowerCase()
                if (lowerKey !== key) {
                    page[lowerKey] = {...page[key]}
                    delete page[key]
                }
            }
        }
        this.save(filePath, {content: json})
    }
}

export {LowerCaseKeys}
