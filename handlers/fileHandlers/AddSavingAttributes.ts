import {Handler} from "../Handler";
import {FileHandler} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
const chalk = require('chalk')

class AddSavingAttributes extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    run(filePath: string) {
        const {$forms, $} = Handler.readPagesAsDom(filePath)
        $forms.each((index, $form) => {
            const $inputs = $('input[type=text],select,textarea', $form)
            const $checkboxes = $('input[type=checkbox]', $form)
            // handle inputs、selectors and textareas
            $inputs.each((index, ipt) => {
                const $ipt = $(ipt)
                if($ipt.attr(':value') && !$ipt.attr(':value').includes('key')){
                    $ipt.addClass(`key-${$ipt.attr(':value').slice(9,-2).replace(/\./g,'-')}`) // like 'key-1-1-1'
                }
                if ($ipt.attr('@blur')) {
                    return
                }
                const valueOf = $ipt.attr(':value')
                if (valueOf && !valueOf.includes('key__')) {
                    $ipt.attr(':class', valueOf.replace('valueOf', 'savingOf'))
                    if ($ipt.attr('type') === 'text' || ipt.name === 'textarea') {
                        $ipt.attr('@blur', '$emit("unFocus")')
                        $ipt.attr('@input', $ipt.attr('@change') as string)
                        $ipt.attr(':disabled', valueOf.replace('valueOf', 'isSavingOf'))
                        // $ipt.addClass(`key-${valueOf.slice(8,-1).replace('.','-')}`) // like 'key-1-1-1'
                        $ipt.removeAttr('@change')
                    }
                }else{
                    console.log('Skipped cause no key: ',chalk.red($ipt.attr('id')))
                }
            })
            // handle checkboxes
            $checkboxes.each((index, cbx) => {
                const $cbx = $(cbx)
                const $img = $cbx.prev()
                if ($img.attr(':savingOf')) {
                    return
                }
                const handleCheck = $img.prop('@click')
                if (handleCheck) {
                    $img.attr(':class', handleCheck.replace('handleCheck', 'savingOf'))
                }
            })
        })
        this.save(filePath, {$})
    }
}

export {AddSavingAttributes}
