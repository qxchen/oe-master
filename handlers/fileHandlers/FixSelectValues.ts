import {Handler} from "../Handler";
import {FileHandler} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
import {questionOfGetTarget} from "../../utils/questionOfGetTarget";

const chalk = require('chalk')

class FixSelectValues extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    private static handleSelect($select: Cheerio, $: CheerioStatic) {
        $select.find('option').each((i, option) => {
            const $option = $(option)
            const text = $option.text()
            if (text) {
                $option.attr('value', text)
            }
        })
        console.log(chalk.blue(`${$select.attr('id')} has been handled .`))
        $select.prepend("<option value='N/A'>N/A</option>")
    }

    async run(filePath: string) {


        const {useReg, useSelector, targetKeyRules} ={useReg: false,
        useSelector: true, targetKeyRules: "select"}
        const {$} = Handler.readPagesAsDom(filePath)
        if (useReg) {
            const regExp = new RegExp(targetKeyRules)
            const $selects = $('select')
            $selects.each((index, select) => {
                const $select = $(select)
                if (regExp.test(`${$select.attr('id')}`)) {
                    FixSelectValues.handleSelect($(select), $)
                }
            })
        } else if (useSelector) {
            const $selects = $(targetKeyRules)
            $selects.each((index, select) => {
                FixSelectValues.handleSelect($(select), $)
            })
        } else {
            const ids: string[] = JSON.parse(targetKeyRules)
            ids.forEach(id => {
                const $target = $(id)
                if ($target) {
                    FixSelectValues.handleSelect($target, $)
                }
            })
        }
        this.save(filePath, {$})
    }
}

export {FixSelectValues}
