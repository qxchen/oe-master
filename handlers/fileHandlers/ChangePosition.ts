import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
import {getTargetKeys} from "../../utils/getTargetKeys";
import inquirer from "inquirer";

class ChangePosition extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string) {
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const keys = await getTargetKeys(json)
        let {PositionOffset} = await inquirer.prompt([{
            type: "input",
            name: 'PositionOffset',
            message: 'Please enter an array of x and y offset. For example: [10, -3] .'
        }])
        const posOffsite = JSON.parse(PositionOffset)
        keys.forEach((key) => {
            Object.keys(json).some(pKey => {
                return Object.keys(json[pKey]).some((k) => {
                    if (k === key) {
                        json[pKey][k].pos = [
                            json[pKey][k].pos[0] + posOffsite[0],
                            json[pKey][k].pos[1] + posOffsite[1],
                        ]
                    }
                })
            })
        })
        this.save(filePath, {content: json})
    }
}

export {ChangePosition}
