// 把用id解析的json 通过模板 编程 title 解析的json
import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";
import {replaceJsonKey} from "../../utils/replaceJsonKey";

const inquirer = require('inquirer')

class ParseNewJsonToOld extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string): Promise<void> {
        const {templatePath} = await inquirer.prompt([{
            type: "input",
            name: 'templatePath',
            message: 'Please Enter path of vue template .'
        }])
        const {workingJsonPath} = await inquirer.prompt([{
            type: "input",
            name: 'workingJsonPath',
            message: 'Please Enter path of current working json file .'
        }])
        let json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const workingJson: OEJSON = workingJsonPath ? JSON.parse(Handler.readFileString(workingJsonPath)) : {}
        const {$, $forms} = Handler.readPagesAsDom(templatePath)
        $forms.children('input,select,textarea').each((i, ele) => {
            const id = $(ele).attr('id')
            if (id.includes('form')) {
                const jsonKey = id.slice(4).split('_').reverse().join('.')
                let newJsonAttr
                const checkedAttr = $(ele).attr(':checked')
                const valueAttr = $(ele).attr(':value')
                if (checkedAttr) {
                    if (checkedAttr.includes('boolean')) {
                        newJsonAttr = checkedAttr.replace('|', '')
                            .replace('boolean', '')
                    } else {
                        newJsonAttr = checkedAttr
                    }
                } else if (valueAttr) {
                    newJsonAttr = valueAttr
                }
                if (newJsonAttr) {
                    const newJsonKey = newJsonAttr.replace(/\s/, '').trim().slice(9, -2)
                    json = replaceJsonKey(json, workingJson, jsonKey, newJsonKey)
                } else {
                    console.log(`${id} skipped.`)
                }
            }
        })
        this.save(filePath, {content: json})
    }
}

export {ParseNewJsonToOld}
