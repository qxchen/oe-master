import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

const inquirer = require("inquirer");

class AddPopulateRule extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string) {
        const {filterName} = await inquirer.prompt([{
            name: 'filterName',
            message: 'Please enter filter name. for example: g28Value. ',
            type: "input"
        }])
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        for (let pageKey in json) {
            const page = json[pageKey]
            for (let key in page) {
                page[key].rule = `{{'${key}' | ${filterName}}}`
            }
        }
        this.save(filePath, {content: json})
    }
}

export {AddPopulateRule}
