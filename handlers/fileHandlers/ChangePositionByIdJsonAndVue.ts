// 已经确定的json文件，用一个新的id-based的模板进行position的替换，不影响rules
import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

const getJsonValue = (json: OEJSON, key) => {
    let result = null
    for (let pKey in json) {
        if (json.hasOwnProperty(pKey)) {
            const page = json[pKey]
            const target = page[key]
            if (target) {
                result = target
                break;
            }
        }
    }
    return result
}

const replaceJsonValue = (json: OEJSON, key, newValue): OEJSON => {
    for (let pKey in json) {
        if (json.hasOwnProperty(pKey)) {
            const page = json[pKey]
            const target = page[key]
            if (target) {
                json[key] = newValue
                break;
            }
        }
    }
    return json
}

const inquirer = require('inquirer')

class ChangePositionByIdJsonAndVue extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    async run(filePath: string): Promise<void> {
        const {templateVuePath} = await inquirer.prompt([{
            type: "input",
            name: 'templateVuePath',
            message: 'Please Enter path of vue template .'
        }])
        const {templateJsonPath} = await inquirer.prompt([{
            type: "input",
            name: 'templateJsonPath',
            message: 'Please Enter path of id-based template json file .'
        }])
        let workingJson: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const templateJson: OEJSON = JSON.parse(Handler.readFileString(templateJsonPath))
        const {$, $forms} = Handler.readPagesAsDom(templateVuePath)
        $forms.children('input,select,textarea').each((i, ele) => {
            const id = $(ele).attr('id')
            if (id.includes('form')) {
                const idJsonKey = id.slice(4).split('_').reverse().join('.')
                let workingJsonAttr
                const checkedAttr = $(ele).attr(':checked')
                const valueAttr = $(ele).attr(':value')
                if (checkedAttr) {
                    if (checkedAttr.includes('boolean')) {
                        workingJsonAttr = checkedAttr.replace('|', '')
                            .replace('boolean', '')
                    } else {
                        workingJsonAttr = checkedAttr
                    }
                } else if (valueAttr) {
                    workingJsonAttr = valueAttr
                }
                if (workingJsonAttr) {
                    const workingJsonKey = workingJsonAttr.replace(/\s/, '').trim().slice(9, -2)
                    console.log(id, idJsonKey, workingJsonKey)
                    for (let pk in workingJson) {
                        if (workingJson.hasOwnProperty(pk)) {
                            const target = workingJson[pk][workingJsonKey]
                            if (target) {
                                target.pos = templateJson[`page${idJsonKey.split('.')[0]}`][idJsonKey].pos
                                break;
                            }
                        }
                    }
                }

            }
        })
        this.save(filePath, {content: workingJson})
    }
}

export {ChangePositionByIdJsonAndVue}
