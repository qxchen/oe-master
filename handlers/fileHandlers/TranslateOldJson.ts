// 处理带 part.x 的json
import {Handler} from "../Handler";
import {FileHandler, OEJSON, OEJSONOld} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

const inquirer = require('inquirer')
const chalk = require('chalk')

class TranslateOldJson extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(true, params);
    }

    private static partReg = /^part\d+$/

    private static async askHandleChoice(partKey: string) {
        const {SkipKeyError} = await inquirer.prompt([
            {
                name: 'SkipKeyError',
                message: `${partKey} doesn't match /^part\\d+$/ . How to handle with all no-matched Keys?`,
                type: 'list',
                choices: [
                    {name: 'Skip all no-matched keys .', value: true},
                    {name: 'Abort', value: false}
                ]
            }
        ])
        return SkipKeyError
    }

    async run(filePath: string) {
        const json: OEJSONOld = JSON.parse(Handler.readFileString(filePath))
        const newJSON: OEJSON = {}
        let skipKeyError = false
        for (let pageKey in json) {
            const page = json[pageKey]
            newJSON[pageKey] = {}
            for (let partKey in page) {
                if (TranslateOldJson.partReg.test(partKey)) {
                    const partOrder = partKey.slice(4)
                    const part = page[partKey]
                    for (let key in part) {
                        newJSON[pageKey][`${partOrder}.${key}`] = {...part[key]}
                    }
                } else if (!skipKeyError) {
                    skipKeyError = await TranslateOldJson.askHandleChoice(partKey)
                    if (!skipKeyError) {
                        console.log(chalk.red('Aborted.'))
                        process.exit()
                    }
                } else {
                    console.log(chalk.gray(`${partKey} Skipped .`))
                }
            }
        }
        this.save(filePath, {content: newJSON})
    }
}

export {TranslateOldJson}
