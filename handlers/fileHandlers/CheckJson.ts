import {Handler} from "../Handler";
import {FileHandler, OEJSON} from "../../types";
import {AllParams} from "../../utils/ParamsGetter";

const chalk = require('chalk')

class CheckJson extends Handler implements FileHandler {
    constructor(params: AllParams) {
        super(false, params);
    }

    run(filePath: string) {
        const json: OEJSON = JSON.parse(Handler.readFileString(filePath))
        const existedKeys: string[] = []
        for (let pageKey in json) {
            if (json.hasOwnProperty(pageKey)) {
                for (let key in json[pageKey]) {
                    if (json[pageKey].hasOwnProperty(key)) {
                        const pos = json[pageKey][key].pos
                        const content = json[pageKey][key].content
                        const rule = json[pageKey][key].rule
                        if (!Array.isArray(pos) || pos === undefined || rule === undefined || content === undefined) {
                            console.log(chalk.red(`Error with ${key}`))
                            if(!Array.isArray(pos) || pos === undefined) console.log(chalk.redBright('Pos error!'))
                            if(rule === undefined) console.log(chalk.redBright('Rule error!'))
                            if(content === undefined) console.log(chalk.redBright('Content error!'))
                        }
                        if (existedKeys.includes(key)) {
                            console.log(chalk.red(`${key} is repetitive!`))
                        }
                        existedKeys.push(key)
                    }
                }
            }
        }
        console.log(chalk.yellow('done'))
    }
}

export {CheckJson}
