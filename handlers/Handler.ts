import {AllParams} from "../utils/ParamsGetter";
import {OEJSON} from "../types";

const cheerio = require('cheerio')
const prettier = require('prettier')
const fs = require('fs')
const chalk = require('chalk')

class Handler {

    constructor(private saveFile: boolean, private params: AllParams) {
    }

    save(originFilePath: string, options?: { $?: CheerioStatic, content?: string | Buffer | Cheerio | OEJSON, newFilePath?: string }): void {
        if (!this.saveFile) return
        const {Rewrite} = this.params
        let {newFilePath, content, $} = options || {}
        if (!Rewrite && !newFilePath) {
            const arr = originFilePath.split('.')
            newFilePath = `${arr.slice(0, arr.length - 1).join('.')}_tmp.${arr[arr.length - 1]}`
        } else {
            newFilePath = originFilePath
        }
        if (typeof content === 'string') {

        } else if (content instanceof Buffer) {

        } else if ($) {
            const newTemplateContent = $('body').html()
            const $wholeFile = cheerio.load(fs.readFileSync(originFilePath, 'utf-8'))
            if (newTemplateContent) {
                newTemplateContent && $wholeFile('template').html(newTemplateContent)
                console.log(chalk.yellow('自动格式化已被弃用，请自行格式化文件内容。'))
                // console.log('prettier version:', prettier.version)
                // const newContentStr = prettier.format($wholeFile('head').html()?.replace(/&apos;/g, '\''), {parser: 'vue'})
                // console.log('after prettier')
                // console.log(newContentStr)
                const newContentStr = $wholeFile('head').html()?.replace(/&apos;/g, '\'')
                newContentStr && fs.writeFileSync(newFilePath, newContentStr)
            }
        } else {
            // JSON
            const str = prettier.format(JSON.stringify(content), {parser: 'json'})
            content && fs.writeFileSync(newFilePath, str)
        }
    }

    protected static readFileString(filePath: string, asString?: boolean) {
        if (asString === undefined) asString = true
        console.log(filePath)
        return fs.readFileSync(filePath, asString ? 'utf-8' : null)
    }

    protected static readPagesAsDom(filePath: string): { $forms: Cheerio, $: CheerioStatic } {
        const fileStr = Handler.readFileString(filePath)
        const templateStr = cheerio.load(fileStr)('template').html()
        const $ = cheerio.load(templateStr as string)
        return {
            $forms: $('.pageArea form'),
            $
        }
    }
}

export {Handler}
