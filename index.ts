#!/usr/bin/env node
import {FileMaster} from "./masters/FileMaster";
import {CrawlerMaster} from "./masters/CrawlerMaster";
import {brand} from "./constants/brand";
const path = require('path')
console.log(path.resolve())
const {program} = require('commander');

program
    .option('-v, --ver', 'version info')

program.parse(process.argv);

if (program.ver) {
    console.log('2.0.0')
    process.exit()
}


const entry = process.env.entry
console.log(brand)

if (entry) {
    const master = new FileMaster(entry)
    master.run()
}else{
    const master = new CrawlerMaster()
    master.run()
}


